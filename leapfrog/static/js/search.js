
var jt_results = new Array();
var us_results = new Array();
var start_results = new Array();
var is_done = false;
var searchHandler;

var api_calls = {"jt": false, "us": false};

//To intialize the search
function initSearch (sh_func, results) {
  api_calls = {"jt": false, "us": false};
  jt_results = [];
  us_results = [];
  is_done = false;
  start_results = (typeof results == "undefined")?[]: results;
  searchHandler = sh_func;
}

//sorting comparator functions
function viewersAsc(a, b) {
  return parseInt(a.viewers) - parseInt(b.viewers);
}
//sorting comparator functions
function viewersDesc(a, b) {
  return parseInt(b.viewers) - parseInt(a.viewers);
}

//combine the results of multiple search API queries
function combine(result_array) {
  var ret_array = new Array();
  for (var i = 0; i < result_array.length; i++) {
    ret_array = ret_array.concat(result_array[i]);
  }
  return ret_array
}

function checkResults(id) {
  if (!(start_results.length)) return false;
  for (var i = 0; i < solr_results.length; i++) {
    if (id == start_results[i].id) {
      start_results.splice(i, 1);
      return true;
    }
  }
  return false;
}

//convert results retrieved from apis into the proper format
function formatResults(results, source) {
  var check;
  var r_array = new Array();
  if (source == 'justin') {
    for (var i = 0; i < results.length; i++) {
      checkResults("jt-" + results[i].id);
      r_array[i] = {
        id: "jt-" + results[i].id,
        userId: results[i].channel.id,
        username: results[i].channel.login,
        title: results[i].title,
        viewers: results[i].channel_count,
        url: results[i].channel.channel_url,
        description: results[i].channel.status + " obtained client side",
        source: "justin",
        pic: "http://static-cdn.jtvnw.net/previews/live_user_" + results[i].channel.login + "-150x113.jpg",
        embed: "" + results[i].channel.embed_code.replace("auto_play=false", "auto_play=true")
      };        
    }      
  }
  else if (source == 'ustream') {
    for (var i = 0; i < results.length; i++) {
      checkResults("us-" + results[i].id);
      r_array[i] = {
        id:           "us-" + results[i].id,
        userId:       results[i].userId,
        username:     results[i].userName,
        title:        results[i].title,
        viewers:      results[i].currentNumberOfViewers,
        url:          results[i].url,
        description:  results[i].description + " obtained client side",
        source:       "ustream",
        pic: "http://static-cdn1.ustream.tv/livethumb/1_"+ results[i].id +"_160x120_b.jpg",
        embed: "" + results[i].embedTag.replace("autoplay=false", "autoplay=true")
      };        
    }
  }
  return r_array;
}

//search using JT's api
function searchJT(query) {
  $.getJSON("http://api.justin.tv/api/stream/search/" + query + ".json?limit=25&jsonp=?",
    function(data) {
      // for (var i = 0; i < data.length; i++) {
      //   $('body').append("jtv -- " + data[i].title + "<br />");
      // }
      searchHandler("searchJT", data);
    }
  ).error(function() { isDone("searchJT", []); });
}

//search using US's api
function searchUS(query) {
  /*
    http://api.ustream.tv/html/stream/recent/search/description:like:test?key=yourDevKey
    http://api.ustream.tv/html/stream/recent/search/title:like:test?key=yourDevKey
    http://api.ustream.tv/html/stream/recent/search/username:like:test?key=yourDevKey
  */
  $.getJSON("http://api.ustream.tv/json/stream/recent/search/title:like:"
            + query + "?key=yourDevKey&callback=?", 
    function (data) {
      if (data) var datalength = data.length;
      else var datalength = 0;
      // for (var i = 0; i < datalength; i++) {
      //   $('body').append("utv -- " + data[i].title + "<br />")
      // }
      searchHandler("searchUS", data);
    }
  );
}