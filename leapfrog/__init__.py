from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
# import requests
# import simplejson as json
# import sunburnt
#from JtvClient import JtvClient

# configuration
DEBUG = True
SECRET_KEY = "avalon's key"

app = Flask(__name__)
app.config.from_object(__name__)

import leapfrog.views

if __name__ == '__main__':
    app.run()
