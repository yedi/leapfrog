from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from leapfrog import app
import updateData as ud

SOLR_SERVER_URL = "http://localhost:8983/solr/"
US_KEY = "C7D0E3730176ADDE7662D9DF6066CB8F"


@app.route('/')
def index():
    #client = JtvClient('3iolcAKxZEZijRjxael8ng', 'N0J63sbXJ79WBSQHYIkpq1CfOgMv3erTuP247zUic')
    #response = client.get('/stream/list.xml')
    #return response.read()
    #r = requests.get('http://api.justin.tv/api/stream/summary.xml')
    #return r.content #"" + str(r.status_code)
    #print r.content
    #return render_template('index.html', url=testurl, vc=vc, cvc=cvc)
    #return redirect(url_for('search'))
    return render_template('index.html', noTopBar=True)


@app.route('/update')
def update():
    ud.updateUS()
    ud.updateJT()
    ud.updateSRL()
    ud.updateBT()
    ud.updateSA()
    ud.updateTC()
    ud.updateO3D()

    # ud.fillInData('blogtv')
    return "Updated"


@app.route('/delete')
def delete():
    ud.deleteData()
    return "Deleted"


@app.route('/search', methods=['GET'])
def search():
    results = []
    squery = ""
    if request.args:
        squery = request.args['s']
        results = ud.solr_search(squery)

    return render_template('search.html', results=list(results), squery=squery)
    #return results[0]['title_t']
    #return request.args['s'].replace(" ", "<br>") + "\nsecondtest"


@app.route('/multistream', methods=['GET'])
def multistream():
    results = []
    query = ""
    if request.args:
        query = request.args['q']
        results = ud.solr_search(query)

    return render_template('multistream.html', results=list(results), squery=query)

"""
import requests
import simplejson as json

@app.route('/tc', methods=['GET'])
def testtc():
    r = requests.get("http://tinychat.com/ajax/bit/rooms/popular_chats?limit=80")
    tcdata = json.loads(r.content)
    return tcdata['html']
"""
