import requests
import simplejson as json
import sunburnt
from lxml import html, etree
#from urlparse import urlparse
#from JtvClient import JtvClient

count = 0
SOLR_SERVER_URL = "http://localhost:8983/solr/"
US_KEY = "C7D0E3730176ADDE7662D9DF6066CB8F"


def findIndex(f, seq):
    """Return first item in sequence where f(item) == True."""
    for i, item in enumerate(seq):
        if f(item):
            return i


#adds stream data to the infostore
def addStreamData(streams, site_from=None):
    documents = []
    if site_from == 'ustream':
        site_abrv = 'us-'
        for stream in streams:
            documents.append({"id": site_abrv + str(stream['id']),
                                "userId": stream['userId'],
                                "username": stream['userName'],
                                "title": stream['title'],
                                "viewers": stream['currentNumberOfViewers'],
                                "url": stream['url'],
                                "description": stream['description'],
                                "source": "ustream",
                                "pic": "http://static-cdn1.ustream.tv/livethumb/1_" + str(stream['id']) + "_160x120_b.jpg",
                                "embed": stream['embedTag'].replace("autoplay=false", "autoplay=true"),
                                "onair": True})

    elif site_from == 'justin':
        site_abrv = 'jt-'
        for stream in streams:
            documents.append({"id": site_abrv + str(stream.get('id', "")),
                                "userId": stream.get('channel', "").get('id', ""),
                                "username": stream.get('channel', "").get('login', ""),
                                "title": stream.get('title', ""),
                                "viewers": stream.get('channel_count', ""),
                                "url": stream.get('channel', "").get('channel_url', ""),
                                "description":    stream.get('category', "") + " " +
                                                    stream.get('meta_game', "") + " " +
                                                    (stream.get('channel', "").get('status', "") or ""),
                                "source": "justin",
                                "pic": "http://static-cdn.jtvnw.net/previews/live_user_" +
                                        str(stream.get('channel', "").get('login', "")) + "-150x113.jpg",
                                "embed": stream.get('channel', "").get('embed_code', "").replace("auto_play=false", "auto_play=true"),
                                "onair": True})
    else:
        documents = streams

    #create solr_interface object
    si = sunburnt.SolrInterface(SOLR_SERVER_URL)
    if documents:
        si.add(documents)
        si.commit()


#sets live = false for every item in the infostore that is from the given source
def removeLive(source):
    si = sunburnt.SolrInterface(SOLR_SERVER_URL)
    oldLive = si.query(si.Q(onair=True) & si.Q(source=source)).paginate(start=0, rows=500).execute()

    documents = []
    if oldLive:
        #map(lambda x: x['onair'] = False, oldLive)
        for doc in oldLive:
            doc['onair'] = False
            documents.append(doc)

        si.add(documents)
        si.commit()


#solr search
def solr_search(squery):
    si = sunburnt.SolrInterface(SOLR_SERVER_URL)
    querylist = squery.split(" ")
    queryobject = None
    for word in querylist:
        queryobject =  (si.Q(queryobject) |
                        si.Q(si.Q(description=word) | 
                        si.Q(title=word) | 
                        si.Q(username=word) |
                        si.Q(searchInfo=word) |
                        si.Q(casters=word) |
                        si.Q(tags=word)))
    results = si.query(queryobject & si.Q(onair=True)).paginate(start=0, rows=400).execute()
    for i, result in enumerate(results):
        result["rank"] = i
    return results


#deletes all data in the solr store
def deleteData():
    si = sunburnt.SolrInterface(SOLR_SERVER_URL)
    si.delete_all()
    si.commit()


#gets stream info given an array of stream objects
def getStreamInfo(streams):
    jt_streams, us_streams, ret_streams = [], [], []
    jt_streams = [stream for stream in streams if stream['site'] == 'justin']
    #us_streams = [stream for stream in streams if stream['site'] == 'ustream']

    if jt_streams:
        site_abrv = 'jt-'
        r = requests.get("http://api.justin.tv/api/stream/list.json?channel=" +
                            ",".join([stream['channel'] for stream in jt_streams]))
        jt_data = json.loads(r.content)

        for jd in jt_data:
            i = findIndex(lambda stream: stream['channel'] == jd.get('channel', "").get('login', ""), jt_streams)
            ret_streams.append({"id": site_abrv + str(jd.get('id', "")),
                                "userId": jd.get('channel', "").get('id', ""),
                                "username": jd.get('channel', "").get('login', ""),
                                "title": jd.get('title', ""),
                                "viewers": jd.get('stream_count', ""),
                                "url": jd.get('channel', "").get('channel_url', ""),
                                "description":    jd.get('category', "") + " " +
                                                    jd.get('meta_game', "") + " " +
                                                    (jd.get('channel').get('status', "") or "") + " " +
                                                    jt_streams[i]['game'] + " " +
                                                    jt_streams[i]['player'],
                                "source": "srl",
                                "pic": "http://static-cdn.jtvnw.net/previews/live_user_" +
                                        str(jd.get('channel', "").get('login', "")) + "-150x113.jpg",
                                "embed": jd.get('channel', "").get('embed_code', "").replace("auto_play=false", "auto_play=true"),
                                "onair": True})
    return ret_streams


def getBtUserInfo(streams):
    stream_objects = []
    for stream in streams:
        r = requests.get('http://www.blogtv.com/people/' + stream)
        page = html.fromstring(r.content)
        userinfo = page.get_element_by_id('ctl00_ContentPlaceHolder1_userInfoDiv', None)
        if not userinfo:
            continue
        pic = userinfo.find_class('BGB')[0][0].get('src')
        user_bio = userinfo.find_class('userAboutDiv') or ""
        if user_bio:
            user_bio = user_bio[0].text_content().split('Bio: ')[1] or ""
        user_tags = userinfo.find_class('userTagsDiv') or ""
        if user_tags:
            user_tags = user_tags[0].text_content().split('Tags: ')[1] or ""

        embed = page.get_element_by_id('LiveEmbed').get('value', "")

        showinfo = page.get_element_by_id('ctl00_ContentPlaceHolder1_showInfoDiv')
        showinfo = showinfo.text_content().split('tion: ')[1].split('Tags: ')
        desc = showinfo[0]
        show_tags = showinfo[1]

        stream_objects.append({ "username": stream,
                                "pic": pic,
                                "description": desc,
                                "tags": show_tags + user_tags,
                                "embed": embed,
                                "searchInfo": user_bio})
    return stream_objects


#functions for getting stream information from various sites
def getSRL():
    streams = []
    site = None
    r = requests.get("http://speedrunslive.com/streams/")
    srlpage = html.fromstring(r.content)
    stream_groups = srlpage.find_class("streamGroup")
    sgHeaders = [str(gh.text_content()) for gh in srlpage.find_class("streamGroupHead")]

    for i, sg in enumerate(stream_groups):
        playerNames = [str(pn.text_content()) for pn in sg.find_class("streamBtn-slide")]
        for j, channel in enumerate(sg.find_class("streamPanel")):
            #channel[0] is object from the source
            #gets the channel name from the data attribute of object
            #channelName = channel[0].get('data').split("channel=")[1]
            if 'justin.tv' in etree.tostring(channel):
                site = 'justin'
                channelName = channel[0].get('data').split("channel=")[1]
            else:
                site = 'ustream'
                continue
            streams.append({"player": playerNames[j],
                            "channel": channelName,
                            "game": sgHeaders[i],
                            "site": site})
    return streams


def getBT(pagenum):
    r = requests.get("http://www.blogtv.com/livenow/Viewers/All/" + str(pagenum))
    btvpage = html.fromstring(r.content)
    ipwrappers = btvpage.find_class("itemPicWrapper")
    itexts = btvpage.find_class("itemText")

    streams = []
    for i, (wrapper, text) in enumerate(zip(ipwrappers, itexts)):
        id = int(wrapper.get('id')[7:])
        #username = text[0].text_content().split(" ")[1]
        username = wrapper[0].get('href').split('/people/')[1]
        title = wrapper[0].get('title').split(" By")[0]
        viewers = text[1].text_content()[9:]
        url = "http://www.blogtv.com" + wrapper[0].get('href')

        streams.append({"id": 'bt-' + str(id),
                        "userId": id,
                        "username": username,
                        "title": title,
                        "viewers": viewers,
                        "url": url,
                        "description": "BlogTV stream by " + username,
                        "source": "blogtv",
                        "onair": True})
    return streams


def getSA(pagenum):
    r = requests.get("http://www.stickam.com/liveStream.do?category=all&filter=popularity&publicCamOff=on&friendsOnly=on&noProfileImage=off&page=" + str(pagenum))
    sapage = html.fromstring(r.content)

    chviewers = sapage.find_class("chViewerNb")
    chimages = sapage.find_class("chImg")
    chtitles = sapage.find_class("chStreamtitle")
    chdescs = sapage.find_class("chDname")
    chpops = sapage.find_class("popWrapper")

    streams = []
    for i in range(len(chviewers)):
        id = int(chpops[i].get('userid'))
        username = chtitles[i][0].text_content()
        title = username + "'s live stream"
        viewers = int(chviewers[i].text_content().replace(',', ""))
        image = chimages[i][0].get('src')
        url = "http://www.stickam.com" + chtitles[i][0].get('href')
        cat = chdescs[i].text_content()
        tags = cat
        embed = '<div style="width:640px;height:595px;overflow:hidden;"><object id="video_190920392" type="application/x-shockwave-flash" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="640" height="575"><param name="movie" value="http://player.stickam.com/stickamPlayer/oc/' + str(id) + '"><param name="allowScriptAccess" value="always"><param name="wmode" value="transparent"><param name="allowfullscreen" value="true"><param name="scale" value="noscale"><param name="quality" value="high"><param name="flashvars" value="autoPlay=1&autoMute=0&showViews=1"><embed src="http://player.stickam.com/stickamPlayer/oc/' + str(id) + '" flashvars="autoPlay=1&autoMute=0&showViews=1" width="640" height="575" wmode="transparent" allowscriptaccess="always" allowfullscreen="true" scale="noscale" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></object><div style="width:640px;height:15px;overflow:hidden;text-align:right;font-size:12px;"><a href="http://www.stickam.com/home.do?source=embed" target="_blank">Video chat by Stickam.com</a></div></div>'

        streams.append({"id": 'sa-' + username,
                        "userId": id,
                        "username": username,
                        "title": title,
                        "viewers": viewers,
                        "url": url,
                        "description": "Stickam stream by " + username,
                        "tags": tags,
                        "pic": image,
                        "embed": embed,
                        "source": "stickam",
                        "onair": True})

    return streams


def getTC(limit=80):
    r = requests.get("http://tinychat.com/ajax/bit/rooms/popular_chats?limit=80")
    tcdata = json.loads(r.content)
    tcpage = html.fromstring(tcdata['html'])
    users = tcpage.find_class('user')

    tcurl = 'http://tinychat.com/'
    picurl = 'http://upload.tinychat.com/pic/'
    streams = []

    def parseUser(x):
        if not x:
            return ""
        if len(x) < 6:
            return ""
        return x.split('=')[1].replace("&", "")


    for i, user in enumerate(users):
        username = user.get('rel')
        viewer_info = user.find_class('user_count')[0].get('title').split(' ')
        chatters = int(viewer_info[0])
        viewers = int(viewer_info[2])

        pics = user.find_class('pics')[0]
        pics_link = ""
        if len(pics) > 0:
            pics_link = pics[0][0].get('src')

        broadcasters = pics_link.split('user')
        broadcasters = map(parseUser, broadcasters)
        casters_str = " ".join(broadcasters)
        tags = user.find_class('tags')

        tag_string = ""
        if tags:
            tags = tags[0]
            try:
                tag_string = "|".join(map(lambda tag: str(tag.text_content()).replace('#', ""), tags))
            except UnicodeEncodeError:
                continue

        desc = 'Tinychat chat room hosted by ' + username + '. '

        embed = '<script type="text/javascript">var tinychat = { room: "' + username + '", colorbk: "0xffffff", join: "auto", api: "list"};</script><script src="http://tinychat.com/js/embed.js"></script><div id="client"><a href="http://tinychat.com">video chat</a> provided by Tinychat</div>'

        streams.append({"id": 'tc-' + username,
                        "userId": 0,
                        "username": username,
                        "title": username + "'s chat room",
                        "viewers": viewers,
                        "url": tcurl + username,
                        "description": desc,
                        "chatters": chatters,
                        "casters": casters_str,
                        "tags": tag_string,
                        "pic": picurl + username,
                        "embed": embed,
                        "source": "tinychat",
                        "onair": True})

    return streams


def getO3D():
    r = requests.get('http://www.own3d.tv/live/?all')
    opage = html.fromstring(r.content)

    streaminfo = opage.find_class('VIDEOS-1grid-box')
    streams = []
    for stream_div in streaminfo:
        a_ele = stream_div[0]
        url = 'http://www.own3d.tv' + a_ele.get('href')
        thumb = a_ele.find_class('VIDEOS-thumbnail')[0]
        id = int(thumb.get('rel'))
        pic = thumb.get('src')
        title = thumb.get('alt')

        d_div = stream_div[1][0]
        game_title = d_div[2].get('title')
        if "offline" in d_div.text_content():
            continue
        viewers = int(d_div.text_content().split('Viewers')[0].rsplit(' ', 2)[1].replace(",", ""))
        username = d_div[5].text_content()

        embed = '<iframe height="270" width="480" frameborder="0" src="http://www.own3d.tv/liveembed/' + str(id) + '"></iframe><br/><a href="http://www.own3d.tv/live/' + str(id) + '">' + title + '</a>'

        streams.append({"id": 'o3d-' + str(id),
                        "userId": id,
                        "username": username,
                        "title": title,
                        "viewers": viewers,
                        "url": url,
                        "description": "stream of " + game_title,
                        "pic": pic,
                        "embed": embed,
                        "source": "own3d",
                        "onair": True})

    return streams


#functions for the solr infostore with different stream sites
def updateJT():
    r = requests.get("http://api.justin.tv/api/stream/list.json?limit=400")
    streams = None
    streams = json.loads(r.content)
    removeLive('justin')
    addStreamData(streams, 'justin')


def updateUS(pages=5):
    streams = []
    for pagenum in range(1, pages + 1):
        r = requests.get("http://api.ustream.tv/json/stream/popular/search/all?key=yourDevKey&page=" + str(pagenum))
        streams.extend(json.loads(r.content)['results'])

    removeLive('ustream')
    addStreamData(streams, 'ustream')


def updateSRL():
    streams = getSRL()
    streams = getStreamInfo(streams)
    removeLive('srl')
    addStreamData(streams)


def updateBT(pages=1):
    streams = []
    for pagenum in range(pages):
        streams.extend(getBT(pagenum))
    removeLive('blogtv')
    addStreamData(streams)


def updateSA(pages=1):
    streams = []
    for pagenum in range(pages):
        streams.extend(getSA(pagenum))
    removeLive('stickam')
    addStreamData(streams)


def updateTC():
    streams = []
    streams = getTC()
    removeLive('tinychat')
    addStreamData(streams)


def updateO3D():
    streams = []
    streams = getO3D()
    removeLive('own3d')
    addStreamData(streams)


def fillInData(source):
    si = sunburnt.SolrInterface(SOLR_SERVER_URL)
    streams = si.query(si.Q(onair=True) & si.Q(source=source)).paginate(start=0, rows=500).execute()

    #map(lambda x: x["username"] streams)

    new_streams = []
    for stream in streams:
        new_streams.append(dict(stream.items() + getBtUserInfo([stream["username"]])[0].items()))

    addStreamData(new_streams)
